use std::{
    io,
    mem::{self, MaybeUninit},
    slice,
};

use windows::Win32::System::{
    LibraryLoader::GetModuleHandleA,
    ProcessStatus::{K32GetModuleInformation, MODULEINFO},
    Threading::GetCurrentProcess,
};

/// Holds information for the module we hook.
#[derive(Debug)]
pub struct Module<'a> {
    name: &'a str,
    memory: &'a [u8],
}

impl<'a> Module<'a> {
    /// Gets a handle to a module from its PE name, e.g. "notepad.exe".
    pub unsafe fn from_pe(name: &'a str) -> Option<Self> {
        let mut module_info = MaybeUninit::<MODULEINFO>::uninit();
        if !K32GetModuleInformation(
            GetCurrentProcess(),
            GetModuleHandleA(name),
            module_info.as_mut_ptr(),
            mem::size_of::<MODULEINFO>() as u32,
        )
        .as_bool()
        {
            None
        } else {
            let module = module_info.assume_init();
            let memory =
                slice::from_raw_parts(module.lpBaseOfDll as *const u8, module.SizeOfImage as usize);

            Some(Self { name, memory })
        }
    }

    /// Gets an immutable reference to this module's name.
    pub fn name(&self) -> &str {
        self.name
    }

    /// Gets the base address of this module in memory.
    pub fn base(&self) -> usize {
        self.memory.as_ptr() as usize
    }

    /// Gets the size of this module in memory, measured in bytes.
    pub fn size(&self) -> usize {
        self.memory.len()
    }

    /// Finds the first address in this module that matches the signature `pattern`
    /// and returns a pointer to the byte at the address.
    ///
    /// The `pattern` is a string where every byte is represented as hexadecimal
    /// characters separated by whitespaces. For wildcard matches where a byte may
    /// be non-constant `??` may be used.
    ///
    /// Example: `AB 01 32 ?? 48`
    pub fn find_signature(&self, pattern: &str) -> io::Result<*const u8> {
        let pattern_elements: Vec<_> = pattern
            .split(' ')
            .map(|e| u8::from_str_radix(e, 16).ok())
            .collect();

        self.memory
            .windows(pattern_elements.len())
            .find(|window| {
                pattern_elements
                    .iter()
                    .zip(window.iter())
                    .all(|(pattern_byte, mem_byte)| {
                        pattern_byte.map_or(true, |pred| pred == *mem_byte)
                    })
            })
            .map(|window| window.as_ptr())
            .ok_or_else(|| {
                io::Error::new(
                    io::ErrorKind::UnexpectedEof,
                    "failed to find signature pattern in memory",
                )
            })
    }
}
