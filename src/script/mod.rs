mod state;
pub use self::state::LuaState;

use std::error::Error;

use crate::{lua, module::Module};

/// The entrypoint to the actual wizscript logic.
pub fn wizscript(module: Module) -> Result<(), Box<dyn Error>> {
    // Find all Lua functions in the binary by signature scanning and gather them.
    let _lua_state = unsafe { lua::LuaEngine::new(&module).and_then(LuaState::new)? };

    println!("Works");
    Ok(())
}
