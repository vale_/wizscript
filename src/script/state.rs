use std::io;

use crate::lua;

/// An abstraction around the raw [`lua::LuaEngine`] to enhance usability of the
/// API without having to go through the C API.
#[derive(Clone)]
pub struct LuaState {
    engine: lua::LuaEngine,

    raw_state: *mut lua::raw::lua_State,
}

impl LuaState {
    /// Attempts to create and allocate a new Lua state from a
    /// [`LuaEngine`][lua::LuaEngine] binding.
    ///
    /// The memory will be freed when instances of this object are dropped.
    pub fn new(engine: lua::LuaEngine) -> io::Result<Self> {
        let raw_state = unsafe { (engine.luaL_newstate)() };
        if raw_state.is_null() {
            Err(io::Error::new(
                io::ErrorKind::OutOfMemory,
                "cannot allocate memory for the raw Lua state",
            ))
        } else {
            Ok(Self { engine, raw_state })
        }
    }
}

impl Drop for LuaState {
    fn drop(&mut self) {
        unsafe { (self.engine.lua_close)(self.raw_state) }
    }
}
