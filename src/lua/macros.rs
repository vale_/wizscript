macro_rules! lua_api {
    ($($name:ident($($tt:tt)*) $(-> $ret:ty)? $(;)*)+) => {
        lua_api_impl!($($name($($tt)*) $(-> $ret)?;)+);
    };
}

macro_rules! lua_api_impl {
    ($($name:ident($($tt:tt)*) $(-> $ret:ty)? $(;)*)+) => {
        lua_api_impl!($(extern "C" $name($($tt)*) $(-> $ret)?;)+);
    };
    ($(extern $conv:literal $name:ident($($tt:tt)*) $(-> $ret:ty)? $(;)*)+) => {
        $(
            #[allow(non_camel_case_types)]
            pub type $name = unsafe extern $conv fn($($tt)*) $(-> $ret)?;
        )+
    };
}
