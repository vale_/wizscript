//! Lua - An Extensible Extension Language

use std::{
    ffi::{c_void, VaList},
    os::raw::{c_char, c_double, c_int, c_ptrdiff_t, c_size_t},
};

/// Type of numbers in Lua
pub type lua_Number = c_double;

/// Type for integer functions
pub type lua_Integer = c_ptrdiff_t;

pub const LUA_VERSION: &str = "Lua 5.1";
pub const LUA_RELEASE: &str = "Lua 5.1.5";
pub const LUA_VERSION_NUM: c_int = 501;
pub const LUA_COPYRIGHT: &str = "Copyright (C) 1994-2012 Lua.org, PUC-Rio";
pub const LUA_AUTHORS: &str = "R. Ierusalimschy, L. H. de Figueiredo & W. Celes";

// Pseudo-indices
pub const LUA_REGISTRYINDEX: c_int = -10000;
pub const LUA_ENVIRONINDEX: c_int = -10001;
pub const LUA_GLOBALSINDEX: c_int = -10002;

#[inline(always)]
pub fn lua_upvalueindex(i: c_int) -> c_int {
    LUA_GLOBALSINDEX - i
}

// Thread status
pub const LUA_OK: c_int = 0;
pub const LUA_YIELD: c_int = 1;
pub const LUA_ERRRUN: c_int = 2;
pub const LUA_ERRSYNTAX: c_int = 3;
pub const LUA_ERRMEM: c_int = 4;
pub const LUA_ERRERR: c_int = 5;

pub type lua_State = c_void;

pub type lua_CFunction = unsafe extern "C" fn(L: *mut lua_State) -> c_int;

/// Function type for reading blocks when loading Lua chunks.
pub type lua_Reader =
    unsafe extern "C" fn(L: *mut lua_State, ud: *mut c_void, sz: *mut c_size_t) -> *const c_char;

/// Function type for writing blocks when dumping Lua chunks.
pub type lua_Writer = unsafe extern "C" fn(
    L: *mut lua_State,
    p: *const c_void,
    sz: c_size_t,
    ud: *mut c_void,
) -> c_int;

/// Prototype for memory-allocation functions.
pub type lua_Alloc = unsafe extern "C" fn(
    ud: *mut c_void,
    ptr: *mut c_void,
    osize: c_size_t,
    nsize: c_size_t,
) -> *mut c_void;

// Basic types
pub const LUA_TNONE: c_int = -1;

pub const LUA_TNIL: c_int = 0;
pub const LUA_TBOOLEAN: c_int = 1;
pub const LUA_TLIGHTUSERDATA: c_int = 2;
pub const LUA_TNUMBER: c_int = 3;
pub const LUA_TSTRING: c_int = 4;
pub const LUA_TTABLE: c_int = 5;
pub const LUA_TFUNCTION: c_int = 6;
pub const LUA_TUSERDATA: c_int = 7;
pub const LUA_TTHREAD: c_int = 8;

pub const LUA_MINSTACK: c_int = 20;

// State manipulation
lua_api! {
    lua_newstate(f: lua_Alloc, ud: *mut c_void) -> *mut lua_State;
    lua_close(L: *mut lua_State);
    lua_newthread(L: *mut lua_State) -> *mut lua_State;

    lua_atpanic(L: *mut lua_State, panicf: lua_CFunction) -> lua_CFunction;
}

// Basic stack manipulation
lua_api! {
    lua_gettop(L: *mut lua_State) -> c_int;
    lua_settop(L: *mut lua_State, idx: c_int);
    lua_pushvalue(L: *mut lua_State, idx: c_int);
    lua_remove(L: *mut lua_State, idx: c_int);
    lua_insert(L: *mut lua_State, idx: c_int);
    lua_replace(L: *mut lua_State, idx: c_int);
    lua_checkstack(L: *mut lua_State, sz: c_int) -> c_int;

    lua_xmove(from: *mut lua_State, to: *mut lua_State, n: c_int);
}

// access functions (stack -> C)
lua_api! {
    lua_isnumber(L: *mut lua_State, idx: c_int) -> c_int;
    lua_isstring(L: *mut lua_State, idx: c_int) -> c_int;
    lua_iscfunction(L: *mut lua_State, idx: c_int) -> c_int;
    lua_isuserdata(L: *mut lua_State, idx: c_int) -> c_int;
    lua_type(L: *mut lua_State, idx: c_int) -> c_int;
    lua_typename(L: *mut lua_State, tp: c_int) -> *const c_char;

    lua_equal(L: *mut lua_State, idx1: c_int, idx2: c_int) -> c_int;
    lua_rawequal(L: *mut lua_State, idx1: c_int, idx2: c_int) -> c_int;
    lua_lessthan(L: *mut lua_State, idx1: c_int, idx2: c_int) -> c_int;

    lua_tonumber(L: *mut lua_State, idx: c_int) -> lua_Number;
    lua_tointeger(L: *mut lua_State, idx: c_int) -> lua_Integer;
    lua_toboolean(L: *mut lua_State, idx: c_int) -> c_int;
    lua_tolstring(L: *mut lua_State, idx: c_int, len: *mut c_size_t) -> *const c_char;
    lua_objlen(L: *mut lua_State, idx: c_int) -> c_size_t;
    lua_tocfunction(L: *mut lua_State, idx: c_int) -> lua_CFunction;
    lua_touserdata(L: *mut lua_State, idx: c_int) -> *mut c_void;
    lua_tothread(L: *mut lua_State, idx: c_int) -> *mut lua_State;
    lua_topointer(L: *mut lua_State, idx: c_int) -> *const c_void;
}

// push functions (C -> stack)
lua_api! {
    lua_pushnil(L: *mut lua_State);
    lua_pushnumber(L: *mut lua_State, n: lua_Number);
    lua_pushinteger(L: *mut lua_State, n: lua_Integer);
    lua_pushlstring(L: *mut lua_State, s: *const c_char, l: c_size_t);
    lua_pushstring(L: *mut lua_State, s: *const c_char);
    lua_pushvfstring(L: *mut lua_State, fmt: *const c_char, argp: VaList) -> *const c_char;
    lua_pushfstring(L: *mut lua_State, fmt: *const c_char, ...) -> *const c_char;
    lua_pushcclosure(L: *mut lua_State, r#fn: lua_CFunction, n: c_int);
    lua_pushboolean(L: *mut lua_State, b: c_int);
    lua_pushlightuserdata(L: *mut lua_State, p: *mut c_void);
    lua_pushthread(L: *mut lua_State) -> c_int;
}

// get functions (Lua -> stack)
lua_api! {
    lua_gettable(L: *mut lua_State, idx: c_int);
    lua_getfield(L: *mut lua_State, idx: c_int, k: *const c_char);
    lua_rawget(L: *mut lua_State, idx: c_int);
    lua_rawgeti(L: *mut lua_State, idx: c_int, n: c_int);
    lua_createtable(L: *mut lua_State, narr: c_int, nrec: c_int);
    lua_newuserdata(L: *mut lua_State, sz: c_size_t) -> *mut c_void;
    lua_getmetatable(L: *mut lua_State, objindex: c_int) -> c_int;
    lua_getfenv(L: *mut lua_State, idx: c_int);
}

// set functions (stack -> Lua)
lua_api! {
    lua_settable(L: *mut lua_State, idx: c_int);
    lua_setfield(L: *mut lua_State, idx: c_int, k: *const c_char);
    lua_rawset(L: *mut lua_State, idx: c_int);
    lua_rawseti(L: *mut lua_State, idx: c_int, n: c_int);
    lua_setmetatable(L: *mut lua_State, objindex: c_int) -> c_int;
    lua_setfenv(L: *mut lua_State, idx: c_int) -> c_int;
}

// `load' and `call' functions (load and run Lua code)
lua_api! {
    lua_call(L: *mut lua_State, nargs: c_int, nresults: c_int);
    lua_pcall(L: *mut lua_State, nargs: c_int, nresults: c_int, errfunc: c_int) -> c_int;
    lua_cpcall(L: *mut lua_State, func: lua_CFunction, ud: *mut c_void) -> c_int;
    lua_load(L: *mut lua_State, reader: lua_Reader, dt: *mut c_void, chunkname: *const c_char) -> c_int;

    lua_dump(L: *mut lua_State, writer: lua_Writer, data: *mut c_void) -> c_int;
}

// Coroutine functions
lua_api! {
    lua_yield(L: *mut lua_State, nresults: c_int) -> c_int;
    lua_resume(L: *mut lua_State, narg: c_int) -> c_int;
    lua_status(L: *mut lua_State) -> c_int;
}

// garbage-collection function and options

pub const LUA_GCSTOP: c_int = 0;
pub const LUA_GCRESTART: c_int = 1;
pub const LUA_GCCOLLECT: c_int = 2;
pub const LUA_GCCOUNT: c_int = 3;
pub const LUA_GCCOUNTB: c_int = 4;
pub const LUA_GCSTEP: c_int = 5;
pub const LUA_GCSETPAUSE: c_int = 6;
pub const LUA_GCSETSTEPMUL: c_int = 7;

lua_api! {
    lua_gc(L: *mut lua_State, what: c_int, data: c_int) -> c_int;
}
