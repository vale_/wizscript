//! Lua standard libraries

use super::raw;

pub const LUA_COLIBNAME: &str = "coroutine";
pub const LUA_TABLENAME: &str = "table";
pub const LUA_IOLIBNAME: &str = "io";
pub const LUA_OSLIBNAME: &str = "os";
pub const LUA_STRLIBNAME: &str = "string";
pub const LUA_MATHLIBNAME: &str = "math";
pub const LUA_DLIBNAME: &str = "debug";
pub const LUA_LOADLIBNAME: &str = "package";

/*extern "C" {
    pub fn luaopen_base(L: *mut c_void) -> c_int;
    pub fn luaopen_table(L: *mut c_void) -> c_int;
    pub fn luaopen_io(L: *mut c_void) -> c_int;
    pub fn luaopen_os(L: *mut c_void) -> c_int;
    pub fn luaopen_string(L: *mut c_void) -> c_int;
    pub fn luaopen_math(L: *mut c_void) -> c_int;
    pub fn luaopen_debug(L: *mut c_void) -> c_int;
    pub fn luaopen_package(L: *mut c_void) -> c_int;
}*/

lua_api! {
    // TODO: Individual libraries.

    luaL_openlibs(L: *mut raw::lua_State);
}
