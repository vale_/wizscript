//! Auxiliary functions for building Lua libraries

use std::{
    ffi::c_void,
    os::raw::{c_char, c_int, c_size_t},
};

use super::raw;

// luaL_getn and luaL_setn are deprecated and so is luaI_openlib.

/// Extra error code for `luaL_load`
pub const LUA_ERRFILE: c_int = raw::LUA_ERRERR + 1;

#[allow(non_camel_case_types)]
#[repr(C)]
pub struct luaL_Reg {
    pub name: *const c_char,
    pub func: raw::lua_CFunction,
}

lua_api! {
    luaI_openlib(L: *mut raw::lua_State, libname: *const c_char, l: *const luaL_Reg, nup: c_int);
    luaL_register(L: *mut raw::lua_State, libname: *const c_char, l: *const luaL_Reg);
    luaL_getmetafield(L: *mut raw::lua_State, obj: c_int, e: *const c_char) -> c_int;
    luaL_callmeta(L: *mut raw::lua_State, obj: c_int, e: *const c_char) -> c_int;
    luaL_typerror(L: *mut raw::lua_State, narg: c_int, tname: *const c_char) -> c_int;
    luaL_argerror(L: *mut raw::lua_State, numarg: c_int, extramsg: *const c_char) -> c_int;
    luaL_checklstring(L: *mut raw::lua_State, numArg: c_int, l: *mut c_size_t) -> *const c_char;
    luaL_optlstring(L: *mut raw::lua_State, numArg: c_int, def: *const c_char, l: *mut c_size_t) -> *const c_char;
    luaL_checknumber(L: *mut raw::lua_State, numArg: c_int) -> raw::lua_Number;
    luaL_optnumber(L: *mut raw::lua_State, nArg: c_int, def: raw::lua_Number) -> raw::lua_Number;

    luaL_checkinteger(L: *mut raw::lua_State, numArg: c_int) -> raw::lua_Integer;
    luaL_optinteger(L: *mut raw::lua_State, nArg: c_int, def: raw::lua_Integer) -> raw::lua_Integer;

    luaL_checkstack(L: *mut raw::lua_State, sz: c_int, msg: *const c_char);
    luaL_checktype(L: *mut raw::lua_State, narg: c_int, t: c_int);
    luaL_checkany(L: *mut raw::lua_State, narg: c_int);

    luaL_newmetatable(L: *mut raw::lua_State, tname: *const c_char) -> c_int;
    luaL_checkudata(L: *mut raw::lua_State, ud: c_int, tname: *const c_char) -> *mut c_void;

    luaL_where(L: *mut raw::lua_State, lvl: c_int);
    luaL_error(L: *mut raw::lua_State, fmt: *const c_char, ...) -> c_int;

    luaL_checkoption(L: *mut raw::lua_State, narg: c_int, def: *const c_char, lst: *const *const c_char) -> c_int;

    luaL_ref(L: *mut raw::lua_State, t: c_int) -> c_int;
    luaL_unref(L: *mut raw::lua_State, t: c_int, r#ref: c_int);

    luaL_loadfile(L: *mut raw::lua_State, filename: *const c_char) -> c_int;
    luaL_loadbuffer(L: *mut raw::lua_State, buff: *const c_char, sz: c_size_t, name: *const c_char) -> c_int;
    luaL_loadstring(L: *mut raw::lua_State, s: *const c_char) -> c_int;

    luaL_newstate() -> *mut raw::lua_State;

    luaL_gsub(L: *mut raw::lua_State, s: *const c_char, p: *const c_char, r: *const c_char) -> *const c_char;

    luaL_findtable(L: *mut raw::lua_State, idx: c_int, fname: *const c_char, szhint: c_int) -> *const c_char;
}

// some useful macros (functions in Rust)

// TODO: Enable once we have everything together.
/*#[allow(non_snake_case)]
#[inline(always)]
pub unsafe fn luaL_argcheck(
    L: *mut raw::lua_State,
    cond: bool,
    numarg: c_int,
    extramsg: *const c_char,
) {
    if !cond {
        luaL_argerror(L, numarg, extramsg);
    }
}

#[allow(non_snake_case)]
#[inline(always)]
pub unsafe fn luaL_checkstring(L: *mut raw::lua_State, n: c_int) -> *const c_char {
    luaL_checklstring(L, n, ptr::null_mut())
}

#[allow(non_snake_case)]
#[inline(always)]
pub unsafe fn luaL_optstring(L: *mut raw::lua_State, n: c_int, d: *const c_char) -> *const c_char {
    luaL_optlstring(L, n, d, ptr::null_mut())
}

#[allow(non_snake_case)]
#[inline(always)]
pub unsafe fn luaL_checkint(L: *mut raw::lua_State, n: c_int) -> c_int {
    luaL_checkinteger(L, n) as c_int
}

#[allow(non_snake_case)]
#[inline(always)]
pub unsafe fn luaL_optint(L: *mut raw::lua_State, n: c_int, d: c_int) -> c_int {
    luaL_optinteger(L, n, d) as c_int
}

#[allow(non_snake_case)]
#[inline(always)]
pub unsafe fn luaL_checklong(L: *mut raw::lua_State, n: c_int) -> c_long {
    luaL_checkinteger(L, n) as c_long
}

#[allow(non_snake_case)]
#[inline(always)]
pub unsafe fn luaL_optlong(L: *mut raw::lua_State, n: c_int, d: c_long) -> c_long {
    luaL_optinteger(L, n, d) as c_long
}

#[allow(non_snake_case)]
#[inline(always)]
pub unsafe fn luaL_typename(L: *mut raw::lua_State, i: c_int) -> *const c_char {
    lua_typename(L, lua_type(L, i))
}

#[allow(non_snake_case)]
#[inline(always)]
pub unsafe fn luaL_dofile(L: *mut raw::lua_State, filename: *const c_char) -> c_int {
    ((luaL_loadfile(L, filename) != 0) || (lua_pcall(L, 0, LUA_MULTRET, 0) != 0)) as c_int
}

#[allow(non_snake_case)]
#[inline(always)]
pub unsafe fn luaL_dostring(L: *mut raw::lua_State, s: *const c_char) -> c_int {
    ((luaL_loadstring(L, s) != 0) || (lua_pcall(L, 0, LUA_MULTRET, 0) != 0)) as c_int
}

#[allow(non_snake_case)]
#[inline(always)]
pub unsafe fn luaL_getmetatable(L: *mut raw::lua_State, n: *const c_char) {
    lua_getfield(L, raw::LUA_REGISTRYINDEX, n)
}

#[allow(non_snake_case)]
#[inline(always)]
pub unsafe fn lua_opt<T, F>(L: *mut raw::lua_State, f: F, n: c_int, d: T) -> T
where
    F: FnOnce(*mut raw::lua_State, c_int) -> T,
{
    if lua_isnoneornil(L, n) {
        d
    } else {
        f(L, n)
    }
}*/

// Generic Buffer manipulation

pub const LUAL_BUFFERSIZE: c_int = 512;

#[allow(non_camel_case_types, non_snake_case)]
#[repr(C)]
pub struct luaL_Buffer {
    /// Current position in buffer
    pub p: *mut c_char,
    /// Number of strings in the stack (level)
    pub lvl: c_int,
    pub L: *mut raw::lua_State,
    pub buffer: [c_char; LUAL_BUFFERSIZE as usize],
}

#[allow(non_snake_case)]
#[inline(always)]
pub unsafe fn luaL_addchar(luaL_prepbuffer: luaL_prepbuffer, B: *mut luaL_Buffer, c: c_char) {
    let start: *mut c_char = (*B).buffer.as_mut_ptr();
    if (*B).p >= start.offset(LUAL_BUFFERSIZE as isize) {
        luaL_prepbuffer(B);
    }

    *(*B).p = c;
    (*B).p = (*B).p.offset(1);
}

// Omit deprecated luaL_putchar.

#[allow(non_snake_case)]
#[inline(always)]
pub unsafe fn luaL_addsize(B: *mut luaL_Buffer, n: c_size_t) {
    (*B).p = (*B).p.add(n);
}

lua_api! {
    luaL_buffinit(L: *mut raw::lua_State, B: *mut luaL_Buffer);
    luaL_prepbuffer(B: *mut luaL_Buffer) -> *mut c_char;
    luaL_addlstring(B: *mut luaL_Buffer, s: *const c_char, l: c_size_t);
    luaL_addstring(B: *mut luaL_Buffer, s: *const c_char);
    luaL_addvalue(B: *mut luaL_Buffer);
    luaL_pushresult(B: *mut luaL_Buffer);
}

// Compatibility with ref system

// pre-defined references
pub const LUA_NOREF: c_int = -2;
pub const LUA_REFNIL: c_int = -1;

// Omit undocumented lua_ref macros. These are replaced by luaL_ref.
