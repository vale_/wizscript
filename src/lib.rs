#![feature(c_size_t, c_variadic)]

#[allow(dead_code)]
mod lua;

mod module;
use self::module::Module;

mod script;
use self::script::wizscript;

use std::{error::Error, ffi::c_void, ptr};

use windows::Win32::{
    Foundation::{BOOL, HINSTANCE},
    System::{
        Console,
        LibraryLoader::DisableThreadLibraryCalls,
        SystemServices::{DLL_PROCESS_ATTACH, DLL_PROCESS_DETACH},
        Threading::{CreateThread, THREAD_CREATE_RUN_IMMEDIATELY},
    },
};

#[cfg(not(all(target_arch = "x86_64", target_os = "windows")))]
compile_error!("Only builds for x64 revisions of Wizard101 are supported");

const PROGRAM: &str = "WizardGraphicalClient.exe";

#[inline(never)]
unsafe extern "system" fn bootstrap_wizscript(arg: *mut c_void) -> u32 {
    let _ = arg;

    // Allocate a console for us so that we can retrieve debugging output.
    if Console::AllocConsole().ok().is_err() {
        return 1;
    }

    // Query module information for the WizardGraphicalClient.
    let module = if let Some(module) = Module::from_pe(PROGRAM) {
        println!(
            "Found {} module at {:#x}, size {:#x}",
            module.name(),
            module.base(),
            module.size()
        );
        module
    } else {
        return 1;
    };

    // We're now executing in a separate thread attached to the game client.
    // We can finally switch to safe Rust and abstract away the `u32` result.
    wizscript(module).is_err() as u32
}

unsafe fn main(module: HINSTANCE, call_reason: u32) -> Result<(), Box<dyn Error>> {
    match call_reason {
        DLL_PROCESS_ATTACH => {
            DisableThreadLibraryCalls(module).ok()?;

            // Bootstrap the wizscript functionality in a separate thread.
            CreateThread(
                ptr::null(),
                0,
                Some(bootstrap_wizscript),
                ptr::null(),
                THREAD_CREATE_RUN_IMMEDIATELY,
                ptr::null_mut(),
            );

            Ok(())
        }
        DLL_PROCESS_DETACH => {
            Console::FreeConsole().ok()?;

            Ok(())
        }
        _ => Ok(()),
    }
}

#[allow(clippy::missing_safety_doc, non_snake_case)]
#[no_mangle]
pub unsafe extern "system" fn DllMain(
    module: HINSTANCE,
    call_reason: u32,
    _reserved: *const (),
) -> BOOL {
    main(module, call_reason).is_ok().into()
}
